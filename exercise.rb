class Exercise

  # Assume that "str" is a sequence of words separated by spaces.
  # Return a string in which every word in "str" that exceeds 4 characters is replaced with "marklar".
  # If the word being replaced has a capital first letter, it should instead be replaced with "Marklar".
  def self.marklar(str)
  
    str.gsub!( /[A-Z][a-z]{4,}/, "42")
  
    str.gsub!( /[a-z]{5,}/, "marklar")
  
    str.gsub!( /42/, "Marklar")
  
    return str
  
  end

  # Return the sum of all even numbers in the Fibonacci sequence, up to
  # the "nth" term in the sequence
  # eg. the Fibonacci sequence up to 6 terms is (1, 1, 2, 3, 5, 8),
  # and the sum of its even numbers is (2 + 8) = 10
  def self.even_fibonacci(nth)

    a = [0]
    c = 0

    nth.times do |i|
        if i==0
            a[i] = 0
            elsif i==1
            a[i] = 1
            else
            a[i] = a[i-1] + a[i-2]
        end
    
        if a[i]%2 == 0
            then c = c + a[i]
        end
    
    end

    return c

  end



end
